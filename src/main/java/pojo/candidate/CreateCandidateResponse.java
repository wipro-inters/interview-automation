package pojo.candidate;

import lombok.Data;

@Data
public class CreateCandidateResponse {
    private String id;
}
