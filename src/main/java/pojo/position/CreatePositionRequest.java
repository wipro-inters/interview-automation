package pojo.position;

import lombok.Data;

@Data
public class CreatePositionRequest {
    private String name;

    public CreatePositionRequest(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "{" +
                "\"name\":\"" + name + "\"" +
                '}';
    }
}
