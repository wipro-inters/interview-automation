package pojo.template;

import lombok.Data;

import java.util.List;

@Data
public class CreateTemplateRequest {
    private String name;
    private List<String> positionsId;
    private List<String> questionsId;

    public CreateTemplateRequest(String name, List<String> positionsId, List<String> questionsId) {
        this.name = name;
        this.positionsId = positionsId;
        this.questionsId = questionsId;
    }

    @Override
    public String toString() {
        return "{" +
                "\"name\":\"" + name + "\"," +
                "\"position\":\"" + positionsId + "\"," +
                "\"question\": " + questionsId +
        "}";
    }
}
