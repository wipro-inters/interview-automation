package pojo.template;

import lombok.Data;
import pojo.PaginationResponse;

import java.util.List;

@Data
public class ListTemplateResponse extends PaginationResponse {
    private List<CreateTemplateResponse> content;

}
