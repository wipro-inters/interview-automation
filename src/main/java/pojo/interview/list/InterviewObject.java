package pojo.interview.list;

import lombok.Data;

@Data
public class InterviewObject {
    private String id;
    private String description;
    private String candidateId;
    private String result;
    private String templateId;

    public InterviewObject() {
    }

    public InterviewObject(String id, String name, String candidateId, String result, String templateId) {
        this.id = id;
        this.description = name;
        this.candidateId = candidateId;
        this.result = result;
        this.templateId = templateId;
    }
}
