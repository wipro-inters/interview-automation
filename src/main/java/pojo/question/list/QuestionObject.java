package pojo.question.list;

import lombok.Data;

@Data
public class QuestionObject {
    private String id;
    private String questionName;

    public QuestionObject() {
    }

    public QuestionObject(String id, String name) {
        this.id = id;
        this.questionName = name;
    }
}
