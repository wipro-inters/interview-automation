package pojo.band;

import lombok.Data;

@Data
public class CreateBandRequest {
    private String name;

    public CreateBandRequest(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "{" +
                "\"name\":\"" + name + "\"" +
                '}';
    }
}
