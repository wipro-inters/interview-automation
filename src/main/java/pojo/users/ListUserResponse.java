package pojo.users;

import lombok.Data;
import pojo.PaginationResponse;

import java.util.List;

@Data
public class ListUserResponse extends PaginationResponse {
    private List<CreateUserResponse> content;
}
