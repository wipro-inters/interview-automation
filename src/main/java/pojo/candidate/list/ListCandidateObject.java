package pojo.candidate.list;

import lombok.Data;

@Data
public class ListCandidateObject {
    private String id;
    private String fullName;

    public ListCandidateObject() {
    }

    public ListCandidateObject(String id, String name) {
        this.id = id;
        this.fullName = name;
    }

}
