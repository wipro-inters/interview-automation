package pojo.interview.create;

import lombok.Data;

import java.util.List;

@Data
public class CreateInterviewRequest {
    private String candidateId;
    private String description;
    private String result;
    private String templateId;
    private List<Scores> scores;

    public CreateInterviewRequest(String candidateId, String description, String result, String templateId, List<Scores> scores) {
        this.candidateId = candidateId;
        this.description = description;
        this.result = result;
        this.templateId = templateId;
        this.scores = scores;
    }

}
