package pojo.subarea;

import lombok.Data;

@Data
public class CreateSubareaResponse {
    private String id;
}
