package api;

import config.Endpoints;
import io.restassured.response.Response;

import static config.Endpoints.BASE_URL;
import static io.restassured.RestAssured.given;

public class Api{

    private static final String CONTENT_TYPE="application/json";

    private Response response;

    public Response create(Endpoints endpoint, Object request){
        response = given().log()
                .all().contentType(CONTENT_TYPE)
                .body(request)
                .post(BASE_URL.getEndpoint()+endpoint.getEndpoint());
        response.prettyPrint();
        return response;
    }

    public Response delete(Endpoints endpoint, String id){
        response = given().log().all().contentType(CONTENT_TYPE)
                .delete(BASE_URL.getEndpoint()+endpoint.getEndpoint()+"/"+id);
        response.prettyPrint();
        return response;
    }

    public Response list(Endpoints endpoint){
        response = given().log().all()
                .get(BASE_URL.getEndpoint()
                +endpoint.getEndpoint()+"?numResults=1000&page=0");
        response.prettyPrint();
        return response;
    }

    public Response update(Endpoints endpoint, String id, Object request){
        response = given().log().all().contentType(CONTENT_TYPE).body(request.toString())
                .put(BASE_URL.getEndpoint()+endpoint.getEndpoint()+"/"+id);
        response.prettyPrint();
        return response;
    }

    public Response details(Endpoints endpoint, String id){
        response = given().log().all().contentType(CONTENT_TYPE)
                .get(BASE_URL.getEndpoint()+endpoint.getEndpoint()+"/"+id);
        response.prettyPrint();
        return response;
    }


}
