package pojo.users;

import lombok.Data;

@Data
public class CreateUserResponse {
    private String id;
}
