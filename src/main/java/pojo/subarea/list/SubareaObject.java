package pojo.subarea.list;

import lombok.Data;

@Data
public class SubareaObject {
    private String id;
    private String name;

    public SubareaObject() {
    }

    public SubareaObject(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
