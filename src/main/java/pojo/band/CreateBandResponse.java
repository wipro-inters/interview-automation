package pojo.band;

import lombok.Data;

@Data
public class CreateBandResponse {
    private String id;
}