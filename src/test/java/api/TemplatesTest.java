package api;

import config.BaseTest;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;
import pojo.template.CreateTemplateRequest;
import pojo.template.CreateTemplateResponse;
import pojo.template.ListTemplateResponse;

import java.util.ArrayList;
import java.util.List;

import static config.Endpoints.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TemplatesTest extends BaseTest {
    private static Response response;
    private static CreateTemplateResponse templateResponse;
    private static CreateTemplateRequest templateRequest;
    private static List<String> positions = new ArrayList<>();
    private static List<String> questions = new ArrayList<>();

    @BeforeAll
    public static void beforeAll(){
        positions = FACTORY.createPositions(3);
        questions = FACTORY.createQuestions(3);
    }

    @AfterAll
    public static void afterAll(){
        for(String position : positions){
            response =  API.delete(POSITION, position);
        }
        for(String question : questions){
            response =  API.delete(QUESTION, question);
        }
    }

    @Test
    @Order(1)
    void createTest(){
        templateRequest = FACTORY.getTemplate(positions, questions);
        response = API.create(TEMPLATE, templateRequest);
        assertEquals(201, response.getStatusCode());
        templateResponse = response.as(CreateTemplateResponse.class);
    }

    @Test
    @Order(2)
    void detailsTest(){
        response = API.details(TEMPLATE, templateResponse.getId());
        assertEquals(200, response.getStatusCode());
    }

    @Test
    @Order(3)
    void listTest() {
        response = API.list(TEMPLATE);
        ListTemplateResponse listResponse = response.as(ListTemplateResponse.class);
        assertEquals(200, response.getStatusCode());
        assertTrue(listResponse.getContent().contains(templateResponse));
    }

    @Test
    @Order(4)
    void updateTest() {
        response = API.update(TEMPLATE, templateResponse.getId(),
                FACTORY.getTemplate(positions, questions));
        assertEquals(201, response.getStatusCode());
    }

    @Test
    @Order(5)
    void deleteTest() {
        response =  API.delete(TEMPLATE, templateResponse.getId());
        assertEquals(204, response.getStatusCode());
    }
}
