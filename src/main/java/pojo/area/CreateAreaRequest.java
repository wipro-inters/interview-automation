package pojo.area;

import lombok.Data;

@Data
public class CreateAreaRequest {
    private String name;

    public CreateAreaRequest(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "{" +
                "\"name\":\"" + name + "\"" +
                '}';
    }
}
