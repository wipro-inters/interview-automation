package api;

import config.BaseTest;
import io.restassured.response.Response;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import pojo.roles.CreateRoleRequest;
import pojo.roles.CreateRoleResponse;
import java.util.Arrays;
import java.util.List;

import static config.Endpoints.ROLES;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class RolesTest extends BaseTest {
    private static Response response;
    private static CreateRoleResponse createRoleResponse;
    private static CreateRoleRequest createRoleRequest;

    @Test
    @Order(1)
    void createTest(){
        createRoleRequest = FACTORY.getRole();
        response = API.create(ROLES, createRoleRequest);
        assertEquals(201, response.getStatusCode());
        createRoleResponse = response.as(CreateRoleResponse.class);
    }

    @Test
    @Order(2)
    void listTest() {
        response = API.list(ROLES);
        assertEquals(200, response.getStatusCode());
        List<CreateRoleResponse> listRolesResponse = Arrays.asList(response.as(CreateRoleResponse[].class));
        assertTrue(listRolesResponse.contains(createRoleResponse));
    }

    @Test
    @Order(3)
    void updateTest() {
        response = API.update(ROLES, createRoleResponse.getId(), FACTORY.getRoleToUpdate(createRoleRequest));
        assertEquals(202, response.getStatusCode());
    }

    @Test
    @Order(4)
    void deleteTest() {
        response =  API.delete(ROLES, createRoleResponse.getId());
        assertEquals(204, response.getStatusCode());
    }
}