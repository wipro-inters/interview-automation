package pojo.subarea;

import lombok.Data;

@Data
public class CreateSubareaRequest {
    private String name;

    public CreateSubareaRequest(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "{" +
                "\"name\":\"" + name + "\"" +
                '}';
    }
}
