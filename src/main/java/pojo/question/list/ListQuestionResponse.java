package pojo.question.list;

import lombok.Data;
import pojo.PaginationResponse;

import java.util.List;

@Data
public class ListQuestionResponse extends PaginationResponse {
    private List<QuestionObject> content;
}
