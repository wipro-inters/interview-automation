package config;

public enum Endpoints {
    BASE_URL("http://localhost:8080"),
    AREA("/areas"),
    BAND("/bands"),
    CANDIDATE("/candidates"),
    INTERVIEW("/interview"),
    POSITION("/positions"),
    QUESTION("/questions"),
    ROLES("/roles"),
    SUBAREA("/subareas"),
    TEMPLATE("/templates"),
    USERS("/users");

    private String endpoint;

    Endpoints(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getEndpoint() {
        return endpoint;
    }
}
