package pojo.candidate.list;


import lombok.Data;
import pojo.PaginationResponse;

import java.util.List;

@Data
public class ListCandidateResponse extends PaginationResponse {
   private List<ListCandidateObject> content;



}

