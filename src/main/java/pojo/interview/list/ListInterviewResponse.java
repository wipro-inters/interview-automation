package pojo.interview.list;

import lombok.Data;
import pojo.PaginationResponse;

import java.util.List;

@Data
public class ListInterviewResponse extends PaginationResponse {
    private List<InterviewObject> content;
}
