package api;

import config.BaseTest;
import io.restassured.response.Response;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import pojo.subarea.CreateSubareaRequest;
import pojo.subarea.CreateSubareaResponse;
import pojo.subarea.list.ListSubareaResponse;
import pojo.subarea.list.SubareaObject;

import static config.Endpoints.SUBAREA;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SubareaTest extends BaseTest {
    static Response response;
    static CreateSubareaRequest subareaRequest;
    static CreateSubareaResponse subareaResponse;

    @Test
    @Order(1)
    void createTest(){
        subareaRequest = FACTORY.getSubarea();
        response = API.create(SUBAREA, subareaRequest);
        assertEquals(201, response.getStatusCode());
        subareaResponse = response.as(CreateSubareaResponse.class);
    }

    @Test
    @Order(2)
    void listTest() {
        response = API.list(SUBAREA);
        assertEquals(200, response.getStatusCode());
        ListSubareaResponse listSubareaResponse = response.as(ListSubareaResponse.class);
        assertTrue(listSubareaResponse.getContent().contains(new SubareaObject(subareaResponse.getId(), subareaRequest.getName())));
    }

    @Test
    @Order(3)
    void updateTest() {
        response = API.update(SUBAREA, subareaResponse.getId(), FACTORY.getSubarea());
        assertEquals(202, response.getStatusCode());
    }

    @Test
    @Order(4)
    void deleteTest() {
        response =  API.delete(SUBAREA, subareaResponse.getId());
        assertEquals(204, response.getStatusCode());
    }
}
