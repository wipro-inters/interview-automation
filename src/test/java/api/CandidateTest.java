package api;


import config.BaseTest;
import io.restassured.response.Response;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import pojo.candidate.CreateCandidateRequest;
import pojo.candidate.CreateCandidateResponse;
import pojo.candidate.DetailsCandidateResponse;
import pojo.candidate.list.ListCandidateObject;
import pojo.candidate.list.ListCandidateResponse;

import static config.Endpoints.CANDIDATE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CandidateTest extends BaseTest {
    private static CreateCandidateResponse candidateResponse;
    private static CreateCandidateRequest candidateRequest;
    private static Response response;

    @Test
    @Order(1)
    void createTest() {
        candidateRequest = FACTORY.getCandidate();
        response = API.create(CANDIDATE, candidateRequest);
        assertEquals(201, response.getStatusCode());
        candidateResponse = response.as(CreateCandidateResponse.class);
    }

    @Test
    @Order(2)
    void detailsTest(){
        response = API.details(CANDIDATE, candidateResponse.getId());
        DetailsCandidateResponse detailsResponse = response.as(DetailsCandidateResponse.class);
        assertEquals(detailsResponse.getId(), candidateRequest.getFullName());
    }

    @Test
    @Order(3)
    void listTest() {
        response = API.list(CANDIDATE);
        assertEquals(200, response.getStatusCode());
        ListCandidateResponse listCandidateResponse = response.as(ListCandidateResponse.class);
        assertTrue(listCandidateResponse.getContent().contains(new ListCandidateObject(candidateResponse.getId(), candidateRequest.getFullName())));
    }

    @Test
    @Order(4)
    void updateTest(){
        response = API.update(CANDIDATE, candidateResponse.getId(), FACTORY.getCandidateToUpdate(candidateRequest));
        assertEquals(201, response.getStatusCode());
    }

    @Test
    @Order(5)
    void deleteTest(){
        response =  API.delete(CANDIDATE, candidateResponse.getId());
        assertEquals(204, response.getStatusCode());
    }

}


