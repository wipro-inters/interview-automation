package pojo.area.list;

import lombok.Data;

@Data
public class ListAreaObject {

    private String id;
    private String area;

    public ListAreaObject() {
    }

    public ListAreaObject(String id, String name) {
        this.area = name;
        this.id = id;
    }
}
