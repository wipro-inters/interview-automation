package pojo.candidate;

import lombok.Data;

@Data
public class DetailsCandidateResponse {

    private Long id;
    private String fullName;
    private String birthDate;
    private String email;
    private String graduation;
    private String registrationDate;

}
