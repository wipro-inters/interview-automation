package pojo.area.list;

import lombok.Data;
import pojo.PaginationResponse;

import java.util.List;

@Data
public class ListAreaResponse extends PaginationResponse {
    private List<ListAreaObject> content;

}
