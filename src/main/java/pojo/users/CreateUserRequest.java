package pojo.users;

import lombok.Data;

@Data
public class CreateUserRequest {
     private String birthDate;
     private String email;
     private String firstName;
     private String lastName;
     private String roleName;
     private String userName;

    public CreateUserRequest(String birthDate, String email, String firstName, String lastName, String roleName, String userName) {
        this.birthDate = birthDate;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.roleName = roleName;
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "{" +
                "\"birthDate\":\"" + birthDate + "\"," +
                "\"email\":\"" + email + "\"," +
                "\"firstName\":\"" + firstName + "\"," +
                "\"lastName\":\"" + lastName + "\"," +
                "\"roleName\":\"" + roleName + "\"," +
                "\"userName\":\"" + userName + "\"" +
                '}';
    }
}
