package pojo.roles;

import lombok.Data;

@Data
public class CreateRoleRequest {
    private String name;

    public CreateRoleRequest(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "{" +
                "\"name\":\"" + name + "\"" +
                "}";
    }
}
