package api;

import config.BaseTest;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;
import pojo.candidate.CreateCandidateResponse;
import pojo.interview.create.CreateInterviewRequest;
import pojo.interview.create.CreateInterviewResponse;
import pojo.interview.list.InterviewObject;
import pojo.interview.list.ListInterviewResponse;
import pojo.template.CreateTemplateResponse;

import java.util.List;

import static config.Endpoints.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class InterviewTest extends BaseTest {
    private static CreateCandidateResponse candidateResponse;
    private static CreateTemplateResponse templateResponse;
    private static CreateInterviewResponse interviewResponse;
    private static CreateInterviewRequest interviewRequest;
    private static List<String> questions;
    private static Response response;
    private static final boolean FINAL_TEST = false;

    @BeforeAll
    public static void beforeAll() {
        //createCandidate
        candidateResponse = API.create(CANDIDATE, FACTORY.getCandidate()).as(CreateCandidateResponse.class);
        //CreateTemplate
        questions = FACTORY.createQuestions(3);
        templateResponse = API.create(TEMPLATE, FACTORY.getTemplate(
                FACTORY.createPositions(3),
                questions)).as(CreateTemplateResponse.class);
    }


    @AfterAll
    public static void afterAll() {
        assertEquals(204, API.delete(CANDIDATE, candidateResponse.getId()).getStatusCode());
        assertEquals(204, API.delete(CANDIDATE, templateResponse.getId()).getStatusCode());
    }

    @Test
    @Order(1)
    void createTest() {
        interviewRequest = FACTORY.getInterview(candidateResponse.getId(), templateResponse.getId(), questions );
        response = API.create(INTERVIEW, interviewRequest);
        assertEquals(201, response.getStatusCode());
        interviewResponse = response.as(CreateInterviewResponse.class);
    }

    @Test
    @Order(2)
    void detailsTest(){
        response = API.details(INTERVIEW, interviewResponse.getId());
        assertEquals(200, response.getStatusCode());
    }

    @Test
    @Order(3)
    void listTest() {
        response = API.list(INTERVIEW);
        assertEquals(200, response.getStatusCode());
        ListInterviewResponse listInterviewResponse = response.as(ListInterviewResponse.class);
        assertTrue(listInterviewResponse.getContent().contains(new InterviewObject(interviewResponse.getId(), interviewRequest.getDescription(), interviewResponse.getId(), interviewResponse.getId(), interviewResponse.getId())));
    }

    @Test
    @Order(4)
    void updateTest() {
        response = API.update(INTERVIEW, interviewResponse.getId(),FACTORY.getInterviewToUpdate(interviewRequest));
        assertEquals(201, response.getStatusCode());
    }

    @Test
    @Order(5)
    void deleteTest(){
        response =  API.delete(INTERVIEW, interviewResponse.getId());
        assertEquals(204, response.getStatusCode());
    }
}

