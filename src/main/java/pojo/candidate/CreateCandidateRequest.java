package pojo.candidate;

import lombok.Data;

@Data
public class CreateCandidateRequest {
    private String birthDate;
    private String email;
    private String fullName;
    private String graduation;

    public CreateCandidateRequest(String birthDate, String email, String fullName, String graduation) {
        this.birthDate = birthDate;
        this.email = email;
        this.fullName = fullName;
        this.graduation = graduation;
    }

    @Override
    public String toString() {
        return "{" +
                "\"birthDate\":\"" + birthDate + "\"," +
                "\"email\":\"" + email + "\"," +
                "\"fullName\":\"" + fullName + "\"," +
                "\"graduation\":\"" + graduation + "\"" +
                '}';
    }
}
