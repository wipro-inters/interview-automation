package pojo.subarea.list;

import lombok.Data;
import pojo.PaginationResponse;

import java.util.List;

@Data
public class ListSubareaResponse extends PaginationResponse {
    private List<SubareaObject> content;
}
