package pojo.template;

import lombok.Data;

@Data
public class CreateTemplateResponse {
    private String id;
}
