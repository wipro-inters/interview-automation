package pojo.roles;

import lombok.Data;

@Data
public class CreateRoleResponse {
    private String id;
}
