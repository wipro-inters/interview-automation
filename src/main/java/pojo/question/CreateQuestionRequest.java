package pojo.question;

import lombok.Data;

@Data
public class CreateQuestionRequest {
    private String answer;
    private String subareaId;
    private String bandId;
    private String questionName;
    private String questionWeight;

    public CreateQuestionRequest(String answer, String subareaId, String bandId, String questionName, String questionWeight) {
        this.answer = answer;
        this.subareaId = subareaId;
        this.bandId = bandId;
        this.questionName = questionName;
        this.questionWeight = questionWeight;
    }

    @Override
    public String toString() {
        return "{" +
                "\"answer\":\"" + answer + "\"," +
                "\"subareaId\":\"" + subareaId + "\"," +
                "\"bandId\":\"" + bandId + "\"," +
                "\"questionName\":\"" + questionName + "\"," +
                "\"questionWeight\":\"" + questionWeight + "\"," +
                '}';
    }
}

