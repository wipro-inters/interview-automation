package api;

import config.BaseTest;
import io.restassured.response.Response;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import pojo.position.CreatePositionRequest;
import pojo.position.CreatePositionResponse;
import pojo.position.ListPositionResponse;

import java.util.Arrays;
import java.util.List;

import static config.Endpoints.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PositionTest extends BaseTest {
    private static CreatePositionResponse positionResponse;
    private static CreatePositionRequest positionRequest;
    private static Response response;
    private static final boolean FINAL_TEST=false;

    @Test
    @Order(1)
    void createTest(){
        positionRequest = FACTORY.getPosition();
        response = API.create(POSITION, positionRequest);
        assertEquals(201, response.getStatusCode());
        positionResponse = response.as(CreatePositionResponse.class);
    }

    @Test
    @Order(2)
    void detailsTest(){
        response = API.details(POSITION, positionResponse.getId());
        assertEquals(200, response.getStatusCode());
        }

    @Test
    @Order(3)
    void listTest() {
        response = API.list(POSITION);
        List<ListPositionResponse> listPositionResponse = Arrays.asList(response.as(ListPositionResponse[].class));
        assertEquals(200, response.getStatusCode());
        assertTrue(listPositionResponse.contains(new ListPositionResponse(positionResponse.getId(),positionRequest.getName())));
    }

    @Test
    @Order(4)
    void updateTest() {
        Response response = API.update(POSITION, positionResponse.getId(),FACTORY.getPositionToUpdate(positionRequest));
        assertEquals(201, response.getStatusCode());
    }

    @Test
    @Order(5)
    void deleteTest() {
        Response response =  API.delete(POSITION, positionResponse.getId());
        assertEquals(204, response.getStatusCode());
    }
}
