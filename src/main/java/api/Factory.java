package api;

import org.apache.commons.lang3.RandomStringUtils;
import pojo.area.CreateAreaRequest;
import pojo.band.CreateBandRequest;
import pojo.candidate.CreateCandidateRequest;
import pojo.interview.create.CreateInterviewRequest;
import pojo.interview.create.Scores;
import pojo.position.CreatePositionRequest;
import pojo.position.CreatePositionResponse;
import pojo.question.CreateQuestionRequest;
import pojo.question.CreateQuestionResponse;
import pojo.roles.CreateRoleRequest;
import pojo.subarea.CreateSubareaRequest;
import pojo.subarea.CreateSubareaResponse;
import pojo.template.CreateTemplateRequest;
import pojo.users.CreateUserRequest;

import java.util.ArrayList;
import java.util.List;

import static config.Endpoints.*;

public class Factory {
    private static final Api API = new Api();

    public CreateRoleRequest getRole(){
        return new CreateRoleRequest(RandomStringUtils.randomAlphabetic(5));
    }

    public CreateUserRequest getUser(String role){
        return new CreateUserRequest(
            "1990-01-01",
            RandomStringUtils.randomAlphabetic(5)+"@email.com",
            RandomStringUtils.randomAlphabetic(5),
            RandomStringUtils.randomAlphabetic(5),
            role,
            RandomStringUtils.randomAlphabetic(5));
    }

    public CreateAreaRequest getArea() {
        return new CreateAreaRequest(RandomStringUtils.randomAlphabetic(5));
    }

    public CreateSubareaRequest getSubarea() {
        return new CreateSubareaRequest(RandomStringUtils.randomAlphabetic(5));
    }

    public CreateBandRequest getBand() {
        return new CreateBandRequest(RandomStringUtils.randomAlphabetic(5));
    }

    public CreateQuestionRequest getQuestion(String subarea, String band){
        return new CreateQuestionRequest(
            RandomStringUtils.randomAlphabetic(10),
            subarea,
            band,
            RandomStringUtils.randomAlphabetic(5), "5");
    }

    public CreatePositionRequest getPosition() {
        return new CreatePositionRequest(RandomStringUtils.randomAlphabetic(5));
    }

    public CreatePositionRequest getPositionToUpdate(CreatePositionRequest positionRequest) {
        positionRequest.setName(RandomStringUtils.randomAlphabetic(5));
        return positionRequest;
    }

    public CreateTemplateRequest getTemplate(List<String> positions, List<String> questions){
        return new CreateTemplateRequest(RandomStringUtils.randomAlphabetic(5), positions, questions);
    }

    public CreateCandidateRequest getCandidate(){
        return new CreateCandidateRequest(
                "1993-12-05",
                RandomStringUtils.randomAlphabetic(5)+"@test.com",
                RandomStringUtils.randomAlphabetic(5),
                RandomStringUtils.randomAlphabetic(5));
    }

    public CreateInterviewRequest getInterview(String candidateId, String templateId, List<String> questions){
        List<Scores> scores = new ArrayList<>();
        for(String question : questions) {
            scores.add(new Scores("0", question));
        }
        return new CreateInterviewRequest( candidateId,
                RandomStringUtils.randomAlphabetic(5),
                "8.0",
                templateId,
                scores);
    }

    public List<String> createPositions(Integer num){
        List<String> positions = new ArrayList<>();
        for(int i=0; i<num; i++){
            positions.add(API.create(POSITION, getPosition()).as(CreatePositionResponse.class).getId());
        }
        return positions;
    }

    public List<String> createQuestions(Integer num){
        List<String> questions = new ArrayList<>();
        for(int i=0; i<num; i++){
            questions.add(API.create(QUESTION, getQuestion(
                    API.create(SUBAREA, getSubarea()).as(CreateSubareaResponse.class).getId(),
                    "1"
            )).as(CreateQuestionResponse.class).getId());
        }

        return questions;
    }

    public CreateUserRequest getUserToUpdate(CreateUserRequest userRequest) {
        userRequest.setFirstName(RandomStringUtils.randomAlphabetic(4));
        userRequest.setLastName(RandomStringUtils.randomAlphabetic(4));
        return userRequest;
    }

    public CreateRoleRequest getRoleToUpdate(CreateRoleRequest roleRequest) {
        roleRequest.setName(RandomStringUtils.randomAlphabetic(4));
        return roleRequest;
    }
    public CreateAreaRequest getAreaToUpdate(CreateAreaRequest areaRequest) {
        areaRequest.setName(RandomStringUtils.randomAlphabetic(4));
        return areaRequest;
    }

    public CreateCandidateRequest getCandidateToUpdate(CreateCandidateRequest candidateRequest){
        candidateRequest.setFullName(RandomStringUtils.randomAlphabetic(4));
        candidateRequest.setEmail(RandomStringUtils.randomAlphabetic(4));
        return candidateRequest;
    }

    public CreateQuestionRequest getQuestionToUpdate(CreateQuestionRequest questionRequest){
        questionRequest.setQuestionName(RandomStringUtils.randomAlphabetic(10));
        questionRequest.setAnswer(RandomStringUtils.randomAlphabetic(5));
        return questionRequest;
    }

    public CreateInterviewRequest getInterviewToUpdate(CreateInterviewRequest interviewRequest){
        interviewRequest.setDescription(RandomStringUtils.randomAlphabetic(10));
        return interviewRequest;
    }
}
