package pojo.interview.create;

import lombok.Data;

@Data
public class Scores{
    private String grade;
    private String questionId;

    public Scores(String grade, String questionId) {
        this.grade = grade;
        this.questionId = questionId;
    }
}

