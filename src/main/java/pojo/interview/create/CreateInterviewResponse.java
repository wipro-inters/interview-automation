package pojo.interview.create;

import lombok.Data;

@Data
public class CreateInterviewResponse {
    private String id;
}
