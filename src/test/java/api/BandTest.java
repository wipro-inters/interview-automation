package api;

import config.BaseTest;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import pojo.band.list.ListBandResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static config.Endpoints.BAND;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BandTest extends BaseTest {
    private static Response response;

    @Test
    void listTest() {
        response = API.list(BAND);
        assertEquals(200, response.getStatusCode());
        List<ListBandResponse> listResponse = Arrays.asList(response.as(ListBandResponse[].class));

        List<ListBandResponse> listBand = new ArrayList<>();
        listBand.add(new ListBandResponse("1", "B1"));
        listBand.add(new ListBandResponse("2", "B2"));

        for (ListBandResponse item : listBand){
            assertTrue(listResponse.contains(item),
                    "Erro to find id:"+item.getId()+
                            " name: "+ item.getBand());
        }
    }
}