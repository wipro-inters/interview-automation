package pojo.area;

import lombok.Data;

@Data
public class CreateAreaResponse {
    private String id;
}
