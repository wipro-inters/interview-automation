package api;

import config.BaseTest;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;
import pojo.roles.CreateRoleRequest;
import pojo.roles.CreateRoleResponse;
import pojo.users.CreateUserRequest;
import pojo.users.CreateUserResponse;
import pojo.users.ListUserResponse;

import static config.Endpoints.ROLES;
import static config.Endpoints.USERS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UsersTest extends BaseTest {
    private static CreateRoleResponse role; //usar quando estiver passando o ID no createUser
    private static CreateRoleRequest roleRequest;
    private static CreateUserResponse userResponse;
    private static CreateUserRequest createUserRequest;

    @BeforeAll
    static void beforeAll(){
        roleRequest = FACTORY.getRole();
        role = API.create(ROLES, roleRequest).as(CreateRoleResponse.class);
    }

    @AfterAll
    static void afterAll(){
        API.delete(ROLES, role.getId() );
    }

    @Test
    @Order(1)
    void createTest(){
        createUserRequest = FACTORY.getUser(roleRequest.getName());
        Response response = API.create(USERS, createUserRequest);
        assertEquals(201, response.getStatusCode());
        userResponse = response.as(CreateUserResponse.class);
    }

    @Test
    @Order(2)
    void detailsTest(){
        Response userDetails = API.details(USERS, userResponse.getId());
        assertEquals(200, userDetails.getStatusCode());
    }

    @Test
    @Order(3)
    void listTest() {
        Response userList = API.list(USERS);
        ListUserResponse listUserResponse = userList.as(ListUserResponse.class);
        assertEquals(200, userList.getStatusCode());
        assertTrue(listUserResponse.getContent().contains(userResponse));
    }

    @Test
    @Order(4)
    void updateTest() {
        Response response = API.update(USERS, userResponse.getId(),FACTORY.getUserToUpdate(createUserRequest));
        assertEquals(201, response.getStatusCode());
    }

    @Test
    @Order(5)
    void deleteTest() {
        Response response =  API.delete(USERS, userResponse.getId());
        assertEquals(204, response.getStatusCode());
    }
}
