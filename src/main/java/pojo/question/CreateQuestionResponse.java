package pojo.question;

import lombok.Data;

@Data
public class CreateQuestionResponse {
    private String id;
}
