package api;

import config.BaseTest;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;
import pojo.area.CreateAreaRequest;
import pojo.area.CreateAreaResponse;
import pojo.question.CreateQuestionRequest;
import pojo.question.CreateQuestionResponse;
import pojo.question.list.QuestionObject;
import pojo.question.list.ListQuestionResponse;
import pojo.subarea.list.ListSubareaResponse;
import pojo.subarea.list.SubareaObject;

import static config.Endpoints.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class QuestionsTest extends BaseTest {
    private static CreateQuestionResponse questionsResponse;
    private static CreateQuestionRequest questionsRequest;
    private static Response response;
    private static CreateAreaResponse createAreaResponse;
    private static final boolean FINAL_TEST=false;


    @BeforeAll
    public static void beforeAll(){
        CreateAreaRequest createAreaRequest = FACTORY.getArea();
        createAreaResponse = API.create(AREA,createAreaRequest).as(CreateAreaResponse.class);
    }

    @AfterAll
    public static void afterAll(){
        assertEquals(204, API.delete(AREA,createAreaResponse.getId()).getStatusCode());
    }


    @Test
    @Order(1)
    void createTest(){
        questionsRequest = FACTORY.getQuestion(createAreaResponse.getId(), "1");
        response = API.create(QUESTION, questionsRequest);
        assertEquals(201, response.getStatusCode());
        questionsResponse = response.as(CreateQuestionResponse.class);
    }

    @Test
    @Order(2)
    void detailsTest(){
        response = API.details(QUESTION, questionsResponse.getId());
        assertEquals(200, response.getStatusCode());
    }

    @Test
    @Order(3)
    void listTest() {
        response = API.list(QUESTION);
        assertEquals(200, response.getStatusCode());
        ListQuestionResponse listQuestionResponse = response.as(ListQuestionResponse.class);
        assertTrue(listQuestionResponse.getContent().contains(new QuestionObject(questionsResponse.getId(), questionsRequest.getQuestionName())));
    }
    @Test
    @Order(4)
    void updateTest() {
        response = API.update(QUESTION, questionsResponse.getId(),FACTORY.getQuestionToUpdate(questionsRequest));
        assertEquals(201, response.getStatusCode());
    }


    @Test
    @Order(5)
    void deleteTest(){
        response =  API.delete(QUESTION, questionsResponse.getId());
        assertEquals(204, response.getStatusCode());
    }

}
