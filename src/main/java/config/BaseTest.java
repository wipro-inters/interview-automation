package config;

import api.Api;
import api.Factory;

public abstract class BaseTest {
    protected static final Api API = new Api();
    protected static final Factory FACTORY = new Factory();

    protected BaseTest() {
    }
}
