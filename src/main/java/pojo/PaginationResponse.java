package pojo;

import lombok.Data;

@Data
public class PaginationResponse {
    private Pageable pageable;
    private String last;
    private String totalElements;
    private String totalPages;
    private String size;
    private String number;
    private Sort sort;
    private String first;
    private String numberOfElements;

    @Data
    private static class Sort{
        private String sorted;
        private String unsorted;
    }

    @Data
    private static class Pageable{
        private Sort sort;
        private String offset;
        private String pageSize;
        private String pageNumber;
        private String paged;
        private String unpaged;
    }
}
