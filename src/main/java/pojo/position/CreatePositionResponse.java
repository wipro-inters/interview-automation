package pojo.position;

import lombok.Data;

@Data
public class CreatePositionResponse {
    private String id;
}
