package pojo.users;

import lombok.Data;
import java.util.List;

@Data
public class DetailsUserResponse {
        private String id;
        private String userName;
        private String firstName;
        private String lastName;
        private String email;
        private List<String> birthDate; //TODO Deve retornar String
        private String roleName;

}
