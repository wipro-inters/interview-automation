package pojo.band.list;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ListBandResponse {
    private String id;
    private String band;

    public ListBandResponse(){

    }

    public ListBandResponse(String id, String band) {
        this.id = id;
        this.band = band;
    }
}
