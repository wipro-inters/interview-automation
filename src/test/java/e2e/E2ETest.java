package e2e;

import config.BaseTest;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pojo.area.CreateAreaResponse;
import pojo.band.CreateBandRequest;
import pojo.band.CreateBandResponse;
import pojo.candidate.CreateCandidateResponse;
import pojo.position.CreatePositionResponse;
import pojo.question.CreateQuestionResponse;
import pojo.roles.CreateRoleRequest;
import pojo.roles.CreateRoleResponse;
import pojo.subarea.CreateSubareaResponse;
import pojo.template.CreateTemplateResponse;
import pojo.users.CreateUserResponse;

import java.util.ArrayList;
import java.util.List;

import static config.Endpoints.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class E2ETest extends BaseTest {
    private static Response response;
    private static CreateRoleResponse createRoleResponse;
    private static CreateUserResponse createUserResponse;
    private static CreateAreaResponse createAreaResponse;
    private static CreateSubareaResponse createSubareaResponse;
    private static CreateQuestionResponse createQuestionResponse;
    private static CreateBandResponse createBandResponse;
    private static CreatePositionResponse createPositionResponse;
    private static CreateTemplateResponse createTemplateResponse;
    private static CreateCandidateResponse createCandidateResponse;
    private static List<String> questions = new ArrayList<>();
    private static List<String> positions = new ArrayList<>();

    @BeforeAll
    static void beforeAll(){
        //Cadastrar Role
        CreateRoleRequest createRoleRequest = FACTORY.getRole();
        response = API.create(ROLES, createRoleRequest);
        assertEquals(201, response.getStatusCode());
        createRoleResponse = response.as(CreateRoleResponse.class);
        //Cadastrar Usuario
        response = API.create(USERS, FACTORY.getUser(createRoleRequest.getName()));//TODO mudar quando for passar ID
        assertEquals(201, response.getStatusCode());
        createUserResponse = response.as(CreateUserResponse.class);
        //Cadastrar Area
        response = API.create(AREA, FACTORY.getArea());
        assertEquals(201, response.getStatusCode());
        createAreaResponse = response.as(CreateAreaResponse.class);
        //Cadastrar Subares
        response = API.create(SUBAREA, FACTORY.getSubarea());
        assertEquals(200, response.getStatusCode()); // TODO mudar quando retornar 201
        createSubareaResponse = response.as(CreateSubareaResponse.class);
        //Cadastrar Banda
        CreateBandRequest createBandRequest = FACTORY.getBand();
        response = API.create(BAND, createBandRequest);
        assertEquals(201, response.getStatusCode());
        createBandResponse = response.as(CreateBandResponse.class);
        //Cadastrar Questoes
        response = API.create(QUESTION, FACTORY.getQuestion(createAreaResponse.getId(), createBandRequest.getName()));//TODO alterar quando request enviar id
        assertEquals(201, response.getStatusCode());
        createQuestionResponse = response.as(CreateQuestionResponse.class);
        questions.add(createQuestionResponse.getId());
        response = API.create(QUESTION, FACTORY.getQuestion(createAreaResponse.getId(), createBandRequest.getName()));
        assertEquals(201, response.getStatusCode());
        createQuestionResponse = response.as(CreateQuestionResponse.class);
        questions.add(createQuestionResponse.getId());
    }

    @Test
    void e2eTest(){
        //Cadastrar Position
        response = API.create(POSITION, FACTORY.getPosition());
        assertEquals(201, response.getStatusCode());
        createPositionResponse = response.as(CreatePositionResponse.class);
        positions.add(createPositionResponse.getId());
        //Cadastrar Templates
        response = API.create(TEMPLATE, FACTORY.getTemplate(positions,
                questions));
        assertEquals(201, response.getStatusCode());
        createTemplateResponse = response.as(CreateTemplateResponse.class);

        //Cadastrar Candidato
        response = API.create(CANDIDATE, FACTORY.getCandidate());
        assertEquals(201, response.getStatusCode());
        createCandidateResponse = response.as(CreateCandidateResponse.class);

        //Cadastrar Interview
    }
}
