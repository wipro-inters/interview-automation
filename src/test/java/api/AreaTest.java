package api;

import config.BaseTest;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;
import pojo.area.CreateAreaRequest;
import pojo.area.CreateAreaResponse;
import pojo.area.list.ListAreaObject;
import pojo.area.list.ListAreaResponse;

import java.util.Arrays;
import java.util.List;

import static config.Endpoints.AREA;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AreaTest extends BaseTest {
    private static Response response;
    private static CreateAreaResponse createAreaResponse;
    private static CreateAreaRequest createAreaRequest;

    @Test
    @Order(1)
    void createTest(){
        createAreaRequest = FACTORY.getArea();
        response = API.create(AREA, createAreaRequest);
        assertEquals(201, response.getStatusCode());
        createAreaResponse = response.as(CreateAreaResponse.class);
        assertTrue(true);

    }

    @Test
    @Order(2)
    void listTest(){
       response = API.list(AREA);
       assertEquals(200, response.getStatusCode());
       ListAreaResponse listAreaResponse = response.as(ListAreaResponse.class);
       assertTrue(listAreaResponse.getContent()
               .contains(new ListAreaObject(createAreaResponse.getId(), createAreaRequest.getName())));

    }

    @Test
    @Order(3)
    void updateTest() {
        response = API.update(AREA, createAreaResponse.getId(), FACTORY.getAreaToUpdate(createAreaRequest));
        assertEquals(202, response.getStatusCode());
    }

    @Test
    @Order(4)
    void deleteTest() {
        response = API.delete(AREA, createAreaResponse.getId());
        assertEquals(204, response.getStatusCode());
    }
}
