package pojo.position;

import lombok.Data;

@Data
public class ListPositionResponse {
     private String id;
     private String name;

    public ListPositionResponse() {
    }

    public ListPositionResponse(String id, String name) {
        this.id = id;
        this.name =name;
    }
}
